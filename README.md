# LIT NBA Web Scraper

LIT NBA Web Scraper is a web scraper, that gets 3 points average for each traditional season for a player.

## Installation

Download/Clone the project. Run 'mvn clean package appassembler:assemble' in root folder of the project.

## Usage

```bash
  sh scraper/bin/3PA Luka Doncic
```

## Tech

**Apache Maven:** 3.8.5

**Java:** 17.0.4.1