package com.lit.scraper;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import picocli.CommandLine;

public class ScraperTest {

    private final ByteArrayOutputStream result = new ByteArrayOutputStream();
    private final PrintStream out = System.out;

    @BeforeEach
    public void init() {
        System.setOut(new PrintStream(result));
    }

    @AfterEach
    public void returnStream() {
        System.setOut(out);
    }

    @Test
    public void testWebScraper() {
        new CommandLine(new Scraper()).execute("Luka", "Doncic");

        String test = """
                Player: [Luka Doncic]
                Date: 2022-23    3 Pointers Average: 7.8
                Date: 2021-22    3 Pointers Average: 8.8
                Date: 2020-21    3 Pointers Average: 8.3
                Date: 2019-20    3 Pointers Average: 8.9
                Date: 2018-19    3 Pointers Average: 7.1
                """;

        assertEquals(test.replaceAll("\n", "\r\n"), result.toString());
    }
}
