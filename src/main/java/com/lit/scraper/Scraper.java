package com.lit.scraper;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

@Command(
        name = "3PA",
        version = "1.0",
        description = "Gets 3 Points Average for each traditional split, for given player.")
public class Scraper implements Callable<Integer> {

    @Parameters(paramLabel = "player", arity = "1...", description = "Name of the player")
    String[] player;

    final String COOKIES_SELECTOR = "#onetrust-reject-all-handler";
    final String INPUT_SELECTOR = ".Block_blockAd__1Q_77 .Input_input__7s5ug";
    final String FOUND_SELECTOR = ".Anchor_anchor__cSc3P.RosterRow_playerLink__qw1vG";
    final String STATS_SELECTOR = ".Anchor_anchor__cSc3P.InnerNavTab_link__qUdtl.InnerNavTab_inactive__9R3Uv";

    WebDriver driver;

    public static void main(String[] args) {
        System.exit(new CommandLine(new Scraper()).execute(args));
    }

    @Override
    public Integer call() throws InterruptedException {
        WebDriverManager.chromedriver().setup();

        driver = new ChromeDriver();

        driver.navigate().to("https://www.nba.com/players");

        closeCookies();

        getPlayerStats(Arrays.toString(player).replaceAll("^\\[|]|,", ""));

        extract3PA();

        driver.close();

        return 0;
    }

    /**
     * Closes the "Cookie Policy" pop up
     */
    public void closeCookies() throws InterruptedException {
        Thread.sleep(1000);
        if (existsElement(COOKIES_SELECTOR)) {
            driver.findElement(By.cssSelector(COOKIES_SELECTOR)).click();
        }
    }

    /**
     * Get to the player's stats page
     *
     * @param player Name of the player
     * @exception InterruptedException if thread is interrupted
     */
    public void getPlayerStats(String player) throws InterruptedException {
        Thread.sleep(4000);
        //Get input text box
        if (existsElement(INPUT_SELECTOR)) {
            driver.findElement(By.cssSelector(INPUT_SELECTOR)).sendKeys(player);
            Thread.sleep(3000);
        }

        //kaj ce sta playera z enakim imenom -> naredim se en

        if (existsElement(FOUND_SELECTOR)) {
            driver.findElement(By.cssSelector(FOUND_SELECTOR)).click();
            Thread.sleep(5000);
        }

        if (existsElement(STATS_SELECTOR)) {
            driver.findElement(By.cssSelector(STATS_SELECTOR)).click();
            Thread.sleep(5000);
        }

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500)", "");
        Thread.sleep(5000);
    }

    /**
     * Extracts the 3-pointers average in each (played) traditional split
     */
    public void extract3PA() {
        List<WebElement> rows = driver.findElements(By.cssSelector(".Crom_body__UYOcU"));
        List<WebElement> dates = driver.findElements(By.cssSelector("td.Crom_text__NpR1_"));
        List<WebElement> threePointers = driver.findElements(By.cssSelector(".Crom_body__UYOcU td:nth-child(10)"));
        System.out.println("Player: " + Arrays.toString(player).replaceAll(",", ""));
        for (int i = 0; i < rows.size(); i++) {
            System.out.print("Date: " + dates.get(i).getText() + "    ");
            System.out.println("3 Pointers Average: " + threePointers.get(i).getText());
        }
    }

    /**
     * Get to the players stats page
     *
     * @param cssSelector elements CSS selector
     * @return true, if element exists
     * @exception NoSuchElementException if element doesn't exist, print stackTrace to CLI
     */
    private boolean existsElement(String cssSelector) {
        try {
            driver.findElement(By.cssSelector(cssSelector));
        } catch (NoSuchElementException e) {
            e.printStackTrace();
            //If the action isn't disabling cookies, exit
            if (!cssSelector.equals(COOKIES_SELECTOR)) {
                System.exit(1);
            }
            return false;
        }
        return true;
    }

}